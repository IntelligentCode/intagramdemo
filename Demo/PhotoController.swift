//
//  PhotoController.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import CoreData

class PhotoController: UITableViewController {
    
    @IBOutlet weak var identifier: UILabel!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var filter: UILabel!
    @IBOutlet weak var link: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var comments: UILabel!
    
    var photo: Photo?
    fileprivate let photoHeightRow = UIScreen.main.bounds.size.width+8.0

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.identifier.text = photo?.identifier
        self.caption.text = photo?.caption
        self.likes.text = photo?.likes.stringValue
        self.filter.text = photo?.filter
        self.link.text = photo?.link
        self.date.text = photo?.date.description
        self.comments.text = photo?.comments.stringValue
        
        do {
            picture.image = try PhotoLibrary.shared.photo(photo!.standart)
        } catch {
            PhotoLibrary.shared.downloadPhoto(photo!.standart, completed: downloadImageCompleted)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Photo
    
    func downloadImageCompleted(_ info: [String: AnyObject?]) {
        print(#function)
        guard let link = info["link"] as? String else { return }
        picture.image = try? PhotoLibrary.shared.photo(link)
    }
    
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 { return photoHeightRow }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }

}
