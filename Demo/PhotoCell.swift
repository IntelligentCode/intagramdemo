//
//  PhotoCell.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {
    
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        likes.text = nil
        photo.image = nil
        comments.text = nil
        indicator.stopAnimating()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
