//
//  UpdateListPhoto.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import InstagramKit
import Network
import System

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class UpdateListPhoto: OperationTemplate {

    let url: URL
    var success: (() -> Void)?
    var failure: ((NSError) -> Void)?
    
    fileprivate let queue = OperationQueue()
    fileprivate var parseOperation: ParseOperation!
    
    
    // MARK: - Lifecycle
    
    init(url aURL: URL) {
        self.url = aURL
        super.init(finishInParent: false)
        self.name = "request_operation"
        self.isReady = false
    }
    
    deinit {
//        print("deinit UpdateListPhoto")
    }
    
    
    // MARK: - Actions
    
    override func start() {
        super.start()
        
        let packet = NetworkPacket(url: url)
        let networkOperation = NetworkOperation.request(packet).response(responseNetworkOperation)
        parseOperation = ParseOperation()
        parseOperation.isNewerPhoto = detectRequestNewerPhoto()
        let finishOperation = BlockOperation {
            self.completed()
            self.finish()
        }
        
        parseOperation.addDependency(networkOperation)
        finishOperation.addDependency(parseOperation)
        
        configurationQueue()
        queue.addOperations([networkOperation, parseOperation, finishOperation], waitUntilFinished: false)

    }
    
    fileprivate func completed() {
        OperationQueue.main.addOperation {
            self.success?()
        }
    }
    
    
    // MARK: - Throw Error
    
    fileprivate func throwMessage(_ code: Int, message: String) {
        let error = NSError.CreateErrorMessage(code, message: message)
        throwError(error)
    }
    
    fileprivate func throwError(_ error: NSError) {
        queue.cancelAllOperations()
        finish()
        OperationQueue.main.addOperation {
            self.failure?(error)
        }
    }
    
    
    // MARK: - Configuration
    
    fileprivate func configurationQueue() {
        queue.maxConcurrentOperationCount = 2
        queue.name = "request_update_list_photo"
    }
    
    
    // MARK: - Private
    
    fileprivate func detectRequestNewerPhoto() -> Bool {
        let component = URLComponents(url: url, resolvingAgainstBaseURL: false)
        if component?.queryItems?.filter({ $0.name == "min_id" }).count > 0 { return true }
        return false
    }
}

extension UpdateListPhoto {
    
    public func responseNetworkOperation(result: NetworkOperationResult) {
        if let data = result.data {
            parseOperation.rawData = data
        } else {
            throwMessage(1100, message: "Сервер вернул пустой ответ")
        }
    }
    
}
