//
//  ImagePersistenceManager.swift
//  Demo
//
//  Created by Артём Шляхтин on 10.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit

enum PersistentStorageError : Error {
    case notFound
    case loadWithError
    case saveWithError
}

class ImagePersistentStorage: NSObject {
    
    let cacheDirectory: URL
    
    // MARK: - Lifecycle
    
    override init() {
        let fileManager = FileManager.default
        let URLs = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)
        self.cacheDirectory = URLs.first!
        super.init()
    }

    // MARK: - Actions
    
    func saveImage(_ image: UIImage, filename: String) throws {
        let path = cacheDirectory.appendingPathComponent(filename)
        guard let data = UIImagePNGRepresentation(image) else { throw PersistentStorageError.saveWithError }
        do {
            try data.write(to: path)
        } catch {
            throw PersistentStorageError.saveWithError
        }
    }
    
    func loadImage(_ filename: String) throws -> UIImage {
        let path = cacheDirectory.appendingPathComponent(filename)
        do {
            let data = try Data(contentsOf: path)
            guard let image = UIImage(data: data) else { throw PersistentStorageError.loadWithError }
            return image
        } catch {
            throw PersistentStorageError.notFound
        }
    }
    
    func loadImageByLink(_ link: String) throws -> UIImage {
        guard let url = URL(string: link) else { throw PersistentStorageError.loadWithError }
        let filename = url.lastPathComponent
        return try loadImage(filename)
    }
    
    func moveFrom(_ location: URL, filename: String) throws {
        let fileManager = FileManager.default
        let destinationURL = cacheDirectory.appendingPathComponent(filename)
        let path = destinationURL.path
        if fileManager.fileExists(atPath: path) {
            try fileManager.removeItem(at: destinationURL)
        }
        try fileManager.copyItem(at: location, to: destinationURL)
    }
    
    func clearCachedImage() throws {
        let path = cacheDirectory.path
        let fileManager = FileManager.default
        let images = try fileManager.contentsOfDirectory(atPath: path)
        try images.forEach { (filename) in
            try fileManager.removeItem(at: cacheDirectory.appendingPathComponent(filename))
        }
    }
    
}
