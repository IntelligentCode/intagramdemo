//
//  Settings+CoreDataProperties.swift
//  Demo
//
//  Created by Артём Шляхтин on 09.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Settings {

    @NSManaged var token: String?
    @NSManaged var user: String?
    @NSManaged var pageMax: String?
    @NSManaged var pageMin: String?

}
