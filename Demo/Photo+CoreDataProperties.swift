//
//  Photo+CoreDataProperties.swift
//  Demo
//
//  Created by Артём Шляхтин on 09.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Photo {

    @NSManaged var identifier: String
    @NSManaged var standart: String
    @NSManaged var thumbnail: String
    @NSManaged var filter: String?
    @NSManaged var likes: NSNumber
    @NSManaged var link: String
    @NSManaged var caption: String?
    @NSManaged var comments: NSNumber
    @NSManaged var username: String?
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    @NSManaged var date: Date

}
