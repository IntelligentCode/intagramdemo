//
//  Settings.swift
//  Demo
//
//  Created by Артём Шляхтин on 09.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import Foundation
import CoreData
import CoreDataHelper

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class Settings: NSManagedObject {
    
    convenience init(context: NSManagedObjectContext) {
        guard let entity = NSEntityDescription.entity(forEntityName: "Settings", in: context) else { fatalError("Entity not found") }
        self.init(entity: entity, insertInto: context)
    }
    
    convenience init() {
        self.init(context: CoreDataHelper.shared.managedObjectContext)
    }

}

extension CoreDataHelper {
    
    func userSettings(_ context: NSManagedObjectContext = CoreDataHelper.shared.managedObjectContext) -> Settings? {
        guard let request = CoreDataHelper.shared.managedObjectModel.fetchRequestTemplate(forName: "UserSettings") else { return nil }
        if let results = try? context.fetch(request) as? [Settings], results?.count > 0 {
            return results?.first
        }
        
        let settings = Settings(context: context)
        do { try CoreDataHelper.shared.saveContext(context, completion: nil) }
        catch { print("Ошибка при создании настроек") }
        return settings
    }
    
}
