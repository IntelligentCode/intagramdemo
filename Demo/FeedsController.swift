//
//  FeedsController.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import CoreData
import InstagramKit
import CoreDataHelper

class FeedsController: UITableViewController {
    
    // MARK: - Properties
    
    fileprivate let countLoadingPhoto = 5
    fileprivate let heightRow = UIScreen.main.bounds.size.width+37.0
    fileprivate var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var userSettingsID: NSManagedObjectID? {
        didSet {
            if  let identifier = userSettingsID,
                let settings = CoreDataHelper.shared.managedObjectContext.object(with: identifier) as? Settings
            {
                userSettings = settings
            }
        }
    }
    
    fileprivate var userSettings: Settings! {
        didSet {
            if CoreDataHelper.shared.isExistPhotoRecord() {
                loadNewerPhotos()
            }
            else {
                initiatePhotos()
            }
        }
    }
    
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.tintColor = UIColor.lightGray
        control.beginRefreshing()
        control.endRefreshing()
        control.addTarget(self, action: #selector(FeedsController.loadNewerPhotos), for: .valueChanged)
        return control
    }()
        
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        initializeFetchedResultsController()
        self.refreshControl = refresh
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Actions
    
    @IBAction func logout(_ sender: AnyObject) {
        let _ = self.navigationController?.popViewController(animated: true)
        CoreDataHelper.shared.userSettings()?.token = nil
        CoreDataHelper.shared.saveContext()
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Photo Segue" {
            let controller = segue.destination as? PhotoController
            controller?.photo = sender as? Photo
        }
    }
    
    
    // MARK: - Feeds Delegate
    
    func feedsUpdatedSuccessfull() {
        self.title = "Instagram"
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.refreshControl?.endRefreshing()
    }
    
    func feedsUpdatedWithError(_ error: NSError) {
        self.title = "Instagram"
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.refreshControl?.endRefreshing()
    }
    

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let photo = fetchedResultsController.object(at: indexPath)
        performSegue(withIdentifier: "Photo Segue", sender: photo)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 0)-1
        if indexPath.row == lastRowIndex { loadMore() }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! PhotoCell
        guard let item = self.fetchedResultsController.object(at: indexPath) as? Photo else { return cell }
        do {
            cell.likes.text = "Лайки: \(item.likes.stringValue)"
            cell.comments.text = "Коментарии: \(item.comments.stringValue)"
            cell.photo.image = try PhotoLibrary.shared.photo(item.thumbnail)
        } catch {
            cell.indicator.startAnimating()
            cell.photo.image = UIImage(named: "Image Placeholder")
            if (self.tableView.isDragging == false && self.tableView.isDecelerating == false) {
                let info = ["indexPath":indexPath]
                PhotoLibrary.shared.downloadPhoto(item.thumbnail, userInfo: info, completed: downloadImageCompleted)
            }
        }
        
        return cell
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.fetchedResultsController.sections, sections.count > section else { return 0 }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightRow
    }
    
    
    // MARK: - Scroll View
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (decelerate == false) {
            loadVisiblePhoto()
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        loadVisiblePhoto()
    }
    
    
    // MARK: - Update Photo
    
    func downloadImageCompleted(_ info: [String: AnyObject?]) {
        guard let indexPath = info["userInfo"]??["indexPath"] as? IndexPath else { return }
        guard let link = info["link"] as? String else { return }
        let cell = self.tableView.cellForRow(at: indexPath) as? PhotoCell
        cell?.photo.image = try? PhotoLibrary.shared.photo(link)
        cell?.indicator.stopAnimating()
    }
    
    func loadVisiblePhoto() {
        let visibleIndexPath = self.tableView.indexPathsForVisibleRows
        visibleIndexPath?.forEach({ (indexPath) in
            guard let item = self.fetchedResultsController.object(at: indexPath) as? Photo else { return }
            do {
                let _ = try PhotoLibrary.shared.photo(item.thumbnail)
            } catch {
                let info = ["indexPath":indexPath]
                PhotoLibrary.shared.downloadPhoto(item.thumbnail, userInfo: info, completed: downloadImageCompleted)
            }
        })
    }
    
    func initiatePhotos() {
        if let token = userSettings.token {
            let url = Instagram.requestPhoto(countLoadingPhoto, token: token)
            loadingPhotosByURL(url)
        }
    }

    func loadNewerPhotos() {
        if let token = userSettings.token, let min = userSettings.pageMin {
            let url = Instagram.requestNewerPhoto(min, count: countLoadingPhoto, token: token)
            loadingPhotosByURL(url)
        }
    }
    
    func loadOlderPhotos() {
        self.title = "Подкгрузка..."
        if let token = userSettings.token, let max = userSettings.pageMax {
            let url = Instagram.requestOlderPhoto(max, count: countLoadingPhoto, token: token)
            loadingPhotosByURL(url)
        }
    }
    
    fileprivate func loadingPhotosByURL(_ url: URL) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        PhotoLibrary.shared.updateFeeds(url: url, successful: feedsUpdatedSuccessfull, failure: feedsUpdatedWithError)
    }
    
    fileprivate func loadMore() {
        loadOlderPhotos()
    }

}


extension FeedsController: NSFetchedResultsControllerDelegate {
    
    func initializeFetchedResultsController() {
        let sortByDate = NSSortDescriptor(key: "date", ascending: false)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Photo")
        request.sortDescriptors = [sortByDate]
        
        let moc = CoreDataHelper.shared.managedObjectContext
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController.delegate = self

        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert: self.tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete: self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case   .move: break
        case .update: break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert: self.tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete: self.tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update: self.tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            self.tableView.deleteRows(at: [indexPath!], with: .fade)
            self.tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
}
