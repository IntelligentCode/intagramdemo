//
//  DownloadImage.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import System

class DownloadImage: OperationTemplate, URLSessionDelegate {
    
    let url: URL
    var completed: ((_ location: URL?) -> Void)?
    fileprivate var session: URLSession?
    
    
    // MARK: - Lifecycle
    
    init(url aURL: URL) {
        self.url = aURL
        super.init(finishInParent: false)
        self.name = "download_image_operation"
    }
    
    convenience init(link: String) {
        let url = URL(string: link)
        self.init(url: url!)
    }
    
    deinit {
//        print("deinit download \(url)")
    }
    
    
    // MARK: - Actions
    
    override func start() {
        super.start()
        
        let sessionConfiguration = URLSessionConfiguration.default
        session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        let downloadFileTask = session?.downloadTask(with: url, completionHandler: completedDownload)
        downloadFileTask?.resume()
    }
    
    
    // MARK: - callback
    
    func completedDownload(location: URL?, response: URLResponse?, error: Error?) {
        completed?(location)
        session?.finishTasksAndInvalidate()
        finish()
    }
    
}
