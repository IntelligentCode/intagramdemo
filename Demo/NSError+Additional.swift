//
//  NSError+Additional.swift
//  Demo
//
//  Created by Артём Шляхтин on 09.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import Foundation

extension NSError {
    
    class func CreateErrorMessage(_ code: Int, message: String) -> NSError {
        let dict = [NSLocalizedDescriptionKey: message]
        let error = NSError(domain: "ru.IntelligentCode.Demo", code: code, userInfo: dict)
        return error
    }
    
}
