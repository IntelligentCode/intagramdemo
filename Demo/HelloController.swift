//
//  HelloController.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import InstagramKit
import CoreData
import CoreDataHelper

class HelloController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Feed Segue" {
            let controller = segue.destination as? FeedsController
            controller?.userSettingsID = sender as? NSManagedObjectID
        }
        
        if segue.identifier == "Authorization Segue" {
            let navigationController = segue.destination as? UINavigationController
            let controller = navigationController?.topViewController as? IGAuthorizationController
            controller?.delegate = self
        }
    }
    
    
    // MARK: - Action
    
    @IBAction func login(_ sender: AnyObject) {
        let userSettings = CoreDataHelper.shared.userSettings()
        if userSettings?.token == nil {
            performSegue(withIdentifier: "Authorization Segue", sender: sender)
        } else {
            performSegue(withIdentifier: "Feed Segue", sender: userSettings?.objectID)
        }
    }
    
    @IBAction func test(_ sender: UIButton) {
        let userSettings = CoreDataHelper.shared.userSettings()
        performSegue(withIdentifier: "Feed Segue", sender: userSettings?.objectID)
    }

}

extension HelloController: InstagramAuthorizationDelegate {
    
    func authorizationSuccessfull(_ token: String, userId: String) {
        
        var userSettings = CoreDataHelper.shared.userSettings()
        if let identifier = userSettings?.user, identifier != userId {
            CoreDataHelper.shared.truncateDatabase()
            userSettings = CoreDataHelper.shared.userSettings()
            PhotoLibrary.shared.clearCachedPhoto()
        }
        
        userSettings?.token = token
        userSettings?.user = userId
        CoreDataHelper.shared.saveContext()
        
        performSegue(withIdentifier: "Feed Segue", sender: userSettings?.objectID)
    }
    
}
