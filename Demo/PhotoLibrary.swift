//
//  PhotoLibrary.swift
//  Demo
//
//  Created by Артём Шляхтин on 10.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import CoreData

class PhotoLibrary: NSObject {
    
    let imageStorage = ImagePersistentStorage()
    fileprivate var downloadImageInProgress = [String: DownloadImage]()
    
    // MARK: - Singletone
    
    static let shared = PhotoLibrary()
    fileprivate override init() { super.init() }
    
    // MARK: - Actions
    
    func updateFeeds(url aURL: URL, successful: (() -> Void)?, failure: ((NSError) -> Void)?) {
        let request = UpdateListPhoto(url: aURL)
        request.success = successful
        request.failure = failure
        request.start()
    }
    
    func downloadPhoto(_ link: String, userInfo: [AnyHashable: Any]? = nil, completed: ((_ info: [String: AnyObject?]) -> Void)? = nil) {
        if downloadImageInProgress[link] != nil { return }
        
        let dict: [String: AnyObject?] = ["link": link as Optional<AnyObject>, "userInfo": userInfo as Optional<AnyObject>]
        let downloadManager = DownloadImage(link: link)
        downloadManager.completed = ({ (location: URL?) in
            self.downloadImageInProgress.removeValue(forKey: link)
            guard let path = location,
                  let name = URL(string: link)?.lastPathComponent else {
                    let error = NSError.CreateErrorMessage(1200, message: "Ошибка загрузки фото")
                    OperationQueue.main.addOperation({
                        completed?(["error": error])
                    })
                    return
            }
            //TODO: доделать обработку ошибки
            do    { try self.imageStorage.moveFrom(path, filename: name) }
            catch { print("Ошибка при копировании фото") }
            OperationQueue.main.addOperation({
                completed?(dict)
            })
        })
        downloadImageInProgress[link] = downloadManager
        downloadManager.start()
    }
    
    func photo(_ link: String) throws -> UIImage {
        return try imageStorage.loadImageByLink(link)
    }
    
    func clearCachedPhoto() {
        do {
            try imageStorage.clearCachedImage()
        } catch {
        }
    }
    
}
