//
//  Photo.swift
//  Demo
//
//  Created by Артём Шляхтин on 08.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import Foundation
import CoreData
import CoreDataHelper

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class Photo: NSManagedObject {
    
    convenience init(context: NSManagedObjectContext) {
        guard let entity = NSEntityDescription.entity(forEntityName: "Photo", in: context) else { fatalError("Entity not found") }
        self.init(entity: entity, insertInto: context)
    }
    
    convenience init() {
        self.init(context: CoreDataHelper.shared.managedObjectContext)
    }

}

extension CoreDataHelper {
    
    func isExistPhotoRecord(_ context: NSManagedObjectContext = CoreDataHelper.shared.managedObjectContext) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Photo")
        request.fetchLimit = 1
        let results = try? context.fetch(request)
        return (results?.count > 0) ? true : false
    }
    
}
