//
//  ParseOperation.swift
//  Demo
//
//  Created by Артём Шляхтин on 09.09.16.
//  Copyright © 2016 Артём Шляхтин. All rights reserved.
//

import UIKit
import System
import CoreData
import CoreDataHelper


class ParseOperation: OperationTemplate {
    
    var isNewerPhoto: Bool = false
    fileprivate let context: NSManagedObjectContext
    
    
    // MARK: - Lifecycle
    
    init() {
        let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        moc.persistentStoreCoordinator = CoreDataHelper.shared.persistentStoreCoordinator
        self.context = moc
        CoreDataHelper.shared.addObserverForContext(context)
        
        super.init(finishInParent: false)
        self.name = "parse_operation"
        self.isReady = false
    }
    
    deinit {
        CoreDataHelper.shared.removeObserverForContext(context)
    }
    
    
    // MARK: - Actions
    
    override func main() {
        super.main()
        
        do {
            if let json = try JSONSerialization.jsonObject(with: rawData, options: .allowFragments) as? [String: AnyObject] {
                parsing(json)
            }
        } catch {
            throwMessage(1002, message: "Ошибка парсера")
        }
        
        finish()
    }
    
    
    // MARK: - Properties
    
    var rawData: Data! {
        didSet {
            if rawData != nil { isReady = true }
        }
    }
    
    
    // MARK: - Message
    
    fileprivate func throwMessage(_ code: Int, message: String) {
        print(message)
    }
    
    
    // MARK: - Core Data
    
    fileprivate func parsing(_ json: [String: AnyObject]) {
        if let nextPage = json["pagination"]?["next_max_id"] as? String {
            parsePagination(nextPage)
        }
        
        if let photos = json["data"] as? [[String: AnyObject]] {
            photos.forEach(appendPhoto)
        }
        
        do {
            try CoreDataHelper.shared.saveContext(context, completion: nil)
        } catch {
            throwMessage(1003, message: "Ошибка при сохранении")
        }
    }
    
    fileprivate func parsePagination(_ page: String) {
        let userSettings = CoreDataHelper.shared.userSettings(context)
        if isNewerPhoto {
            userSettings?.pageMin = page
            if userSettings?.pageMax == nil { userSettings?.pageMax = page }
        } else {
            userSettings?.pageMax = page
            if userSettings?.pageMin == nil { userSettings?.pageMin = page }
        }
    }
    
    fileprivate func appendPhoto(_ object: [String: AnyObject]) {
        guard let photoID = object["id"] as? String, isExistPhotoByID(photoID) == false else { return }
        
        let photo = Photo(context: context)
        photo.identifier = photoID
        
        if let link = object["link"] as? String {
            photo.link = link
        }
        
        if let interval = object["created_time"] as? String, let time = Double(interval) {
            photo.date = Date(timeIntervalSince1970: time)
        }
        
        if let likes = object["likes"]?["count"] as? Int {
            photo.likes = NSNumber(value: likes as Int)
        }
        
        if let comments = object["comments"]?["count"] as? Int {
            photo.comments = NSNumber(value: comments)
        }
        
        if let images = object["images"] as? [String: AnyObject],
           let resolution = images["low_resolution"] as? [String: AnyObject],
           let lowResolution = resolution["url"] as? String
        {
            photo.thumbnail = lowResolution
        }
        
        if let images = object["images"] as? [String: AnyObject],
           let resolution = images["standard_resolution"] as? [String: AnyObject],
           let standartResolution = resolution["url"] as? String
        {
            photo.standart = standartResolution
        }
        
        photo.caption = object["caption"] as? String
        photo.filter = object["filter"] as? String
        photo.username = object["user"]?["full_name"] as? String
    }
    
    fileprivate func isExistPhotoByID(_ identifier: String) -> Bool {
        let object = CoreDataHelper.shared.objectByValue(entityName: "Photo", field: "identifier", value: identifier as AnyObject)
        return (object == nil) ? false : true
    }
    
}
